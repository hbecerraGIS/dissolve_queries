--dissolve
CREATE TABLE public.my_table_dissolve AS
SELECT my_table."field_with_same_values", ST_UNION(my_table.geom)
::geometry(Polygon,EPSG) AS geom
FROM my_table AS my_table
GROUP BY my_table."field_with_same_values"

--polygon to multipolygon
ALTER TABLE public.my_table_dissolve ALTER COLUMN geom
SET DATA TYPE geometry(MultiPolygon) USING ST_MULTI(geom);

-- single dissolve
CREATE TABLE public.dissolve AS
SELECT ST_UNION(geom) FROM my_table

--create dissolve in virtual layer
SELECT ST_UNION(geometry) FROM layer_name

--dissolve in QGIS geometry generator
BUFFER(COLLECT($geometry),0)